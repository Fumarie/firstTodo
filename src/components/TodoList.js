import React, { useState } from 'react';
import TodoForm from "./TodoForm";
import Todo from "./Todo";

const TodoList = () => {
    const [todos, setTodos] = useState([])

    const addTodo = todo => {
        if (!todo.text || /^\s*$/.test(todo.text)) {
            return;
        }

        const newTodos = [todo, ...todos]
        setTodos(newTodos)
    }

    const completeTodo = id => {
        let updatedTodos = todos.map(todo => {
            if(id === todo.id)
                todo.isCompleted = !todo.isCompleted
            return todo
        })
        setTodos(updatedTodos)
    }

    const deleteTodo = id => {
        let removeArr = [...todos].filter(todo => todo.id !== id)
        setTodos(removeArr)
    }

    const updateTodo = (editId, newValue) => {
        if (!newValue.text || /^\s*$/.test(newValue.text)) {
            return
        }
        setTodos(prev => prev.map(item => (item.id === editId ? newValue : item)))
    }

    return (
        <>
            <h1>What's the Plan for Today?</h1>
            <TodoForm onSubmit={addTodo}/>
            <Todo updateTodo={updateTodo} deleteTodo={deleteTodo} completeTodo={completeTodo} todos={todos} />
        </>
    );
};

export default TodoList;
