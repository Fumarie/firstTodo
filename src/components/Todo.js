import React, { useState } from 'react';
import { RiCloseCircleLine } from 'react-icons/ri';
import { TiEdit } from 'react-icons/ti';
import TodoForm from "./TodoForm";

const Todo = ({ todos, completeTodo, deleteTodo, updateTodo }) => {
    const [edit, setEdit] = useState({
        id: null,
        value: ''
    })

    const submitUpdate = value => {
        updateTodo(edit.id, value)
        setEdit({
            id: null,
            value: ''
        })
    }

    if(edit.id) {
        return (
            <TodoForm edit={edit} onSubmit={submitUpdate}/>
        )
    }
    return (
        todos && todos.map((todo, index) =>
            <div
                className={todo.isCompleted ? 'todo-row complete' : 'todo-row'}
                key={index}
            >
                <div
                    onClick={() => completeTodo(todo.id)}
                    key={todo.id}
                >
                    {todo.text}
                </div>
                <div className='icons'>
                    <RiCloseCircleLine
                        onClick={() =>deleteTodo(todo.id)}
                        className='delete-icon'
                    />
                    <TiEdit
                        onClick={() => setEdit({id: todo.id, value: todo.text})}
                        className='edit-icon'
                    />
                </div>
            </div>
        )
    );
};

export default Todo;
