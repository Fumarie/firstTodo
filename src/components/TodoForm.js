import React, { useState, useRef, useEffect } from 'react';

const TodoForm = ({ edit, onSubmit }) => {
    const [title, titleChangeHandler] = useState(edit ? edit.value : '')

    const inputRef = useRef(null);

    useEffect(() => {
        inputRef.current.focus();
    });

    const changeInputHandler = event => {
        titleChangeHandler(event.target.value)
    }

    const onSubmitForm = event => {
        event.preventDefault()
        onSubmit({
            id: Math.floor(Math.random() * 10000),
            text: title
        })
        titleChangeHandler('')
    }

    return (
        <form onSubmit={onSubmitForm} className='todo-form'>
            {!edit ?
            (<>
                <input
                    placeholder='Add a todo'
                    className='todo-input'
                    value={title}
                    onChange={changeInputHandler}
                    ref={inputRef}
                />
                <button type="submit" className='todo-button'>
                    Add todo
                </button>
            </>) : (
                    <>
                        <input
                            placeholder='Update your item'
                            value={title}
                            onChange={changeInputHandler}
                            name='text'
                            ref={inputRef}
                            className='todo-input edit'
                        />
                        <button type="submit" className='todo-button edit'>
                            Update
                        </button>
                    </>
            )}
        </form>
    );
};

export default TodoForm;
